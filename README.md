# Demodam temporary Haven cluster.

Until Wigo4IT takes over this repo hosts the [Haven cluster setup](./terraform) and [Self service user configuration](./configuration) for Demodam.

The [website](./website) https://demodam.nl is also part of this repo.

_Suggested demodam.nl DNS entries_:
- demodam.nl    CNAME demodam.vng.cloud  TTL 1 hour
- *.demodam.nl  CNAME demodam.vng.cloud  TTL 1 hour



---

provisionen van users:
    je maakt op global niveau users (dat zijn 'local users', want geen identity provider) -> die geef je de rol "Base-User" (die kan helemaal niets behalve inloggen) - dit vertaalt zich inderdaad naar kubernetes users door
    daarna maak je op cluster niveau een project aan en die nieuwe global user hang je aan dat cluster scoped project als Member (kan namespaces beheren en aanmaken) of als Owner (kan tevens users managen van het project; beetje tricky) - pas wanneer een Base-User minimaal 1 project heeft, kan 'ie inloggen en is het echt een kubernetes user
vanuit user gezien:
    vervolgens kan de user inloggen en z'n eigen namespaces maken/beheren binnen zijn project(en) - die zich vertalen naar namespaces in het cluster
    ook kan die user z'n kubectl.conf downloaden in de interface, verder alles read only/beperkt


Admin:

Local users adden (geen self service):
    https://rancher.demodam.vng.cloud/g/security/accounts/users   -> rol "User-Base" (kan helemaal niets, behalve projecten die je toewijst)
    https://rancher.demodam.vng.cloud/c/local/projects-namespaces -> assign user aan project met 'Member' rol (kan dan eigen namespaces maken) of Owner (kan ook users managen van project)

** TODO: identity provider inschakelen en met rollen gaan werken, je kunt dan groups aan roles hangen en role aan project als owner denk ik
      komt hier misschien alsnog self service als optie?


User:

Kubeconfig downloaden:
    https://rancher.demodam.vng.cloud/c/local/monitoring

Namespaces beheren (ook toevoegen):
    https://rancher.demodam.vng.cloud/c/local/projects-namespaces
