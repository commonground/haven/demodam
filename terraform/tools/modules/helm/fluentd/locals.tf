locals {
  fluentd_default_values = {}

  fluentd_values = merge(local.fluentd_default_values, var.fluentd_settings)
}
