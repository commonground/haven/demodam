terraform {
  required_version = "= 0.15.5"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "= 2.61.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "= 2.3.1"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "= 2.1.2"
    }
  }
}
