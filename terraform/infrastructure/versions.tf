terraform {
  required_version = "= 1.0.9"

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "= 2.61.0"
    }

    azuread = {
      source  = "hashicorp/azuread"
      version = "= 1.5.0"
    }
  }
}
