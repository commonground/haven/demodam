# Temporary and very raw placeholder for demodam.nl

Yes this needs some love and a CI/CD pipeline.

For now:

## Build

```bash
docker build -t registry.gitlab.com/commonground/haven/demodam/website .
```

## Push

```bash
docker push registry.gitlab.com/commonground/haven/demodam/website
```

## Deploy

```bash
helm upgrade --install -n website website ./helm
```
